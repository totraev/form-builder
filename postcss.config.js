module.exports = {
  plugins: {
    'stylelint': {},
    'postcss-use': {
      modules: [
        'postcss-utilities',
        'postcss-inline-svg',
        'postcss-assets',
        'postcss-autoreset',
        'postcss-font-magician',
        'postcss-will-change'
      ]
    },
    'postcss-cssnext': {},
    'doiuse': {
      browsers: ['last 2 versions', 'not operamini all', 'not IE < 12', 'not bb >= 7'],
      ignore: ['flexbox', 'will-change', 'css3-cursors', 'css-filters', 'pointer-events']
    },
    'postcss-reporter': {
      clearAllMessages: true
    }
  }
}
