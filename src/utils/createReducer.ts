export type Action<Payload> = {
  type: string
  payload?: Payload
  error?: boolean
};

export type HandlersMap<State, Payload> = {
  [actionType: string]: Reducer<State, Payload>
};

export type Reducer<State, Payload> = (state: State, action: Action<Payload>) => State;

function createReducer<S>(handlers: HandlersMap<S, any>, initialState: S): Reducer<S, any> {
  return <Payload = any>(state: S = initialState, action: Action<Payload> = null): S => {
    return handlers[action.type] ?
      handlers[action.type](state, action) :
      state;
  };
}

export default createReducer;

