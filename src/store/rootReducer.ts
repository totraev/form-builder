import { combineReducers } from 'redux-seamless-immutable';
import { reducer as formReducer, FormState } from 'redux-form';

import appReducer, { ImmutableState as AppState } from '../containers/App/duck';

/**
 * Global App state
 */
export type State = {
  form: FormState,
  app: AppState
};

/**
 * Root reducer
 */
export default combineReducers<State>({ 
  form: formReducer,
  app: appReducer
});
