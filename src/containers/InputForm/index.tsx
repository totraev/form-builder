import React from 'react';
import { reduxForm, Form, Field, InjectedFormProps } from 'redux-form';
import { FormGroup, Label, Button } from 'reactstrap';


export type FormDate = {
  input_string: string
};
export type ComponentProps = {
  submitBtn?: string
};
export type Props = ComponentProps & InjectedFormProps<FormDate, ComponentProps>;


class InputForm extends React.Component<Props> {
  static defaultProps = {
    submitBtn: 'Submit'
  };

  render() {
    const { handleSubmit, submitBtn } = this.props;

    return (
      <Form onSubmit={handleSubmit}>
        <FormGroup>
          <Label>JSON</Label>

          <Field
            className="form-control"
            rows={20}
            name="input_string"
            component="textarea"/>
        </FormGroup>

        <Button type="submit">{submitBtn}</Button>
      </Form>
    );
  }
}

export default reduxForm<FormDate, ComponentProps>({ form: 'input_form' })(InputForm);
