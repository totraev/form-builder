import { State } from './duck';
import Immutable from 'seamless-immutable';

export const initialState = Immutable.from<State>({
  error: false,
  errorMsg: '',
  formValues: null,
  json: {
    handler: 'path.to.method',
    initial: {},
    custom_args: {},
    buttons: {
      submit: 'Отправить'
    },
    struct: [
      {
        component: 'TabsBlock',
        id: 'tab_1',
        tabs: [
          {
            verbose_name: 'Вкладка 1',
            icon: 'wrench',
            childs: [
              {
                component: 'FieldSetBlock',
                id: 'field_set',
                legend: 'Блок важных полей',
                childs: [
                  {
                    component: 'StringField',
                    verbose_name: 'Адрес',
                    name: 'field_1',
                    help_text: 'Укажите полный адрес доставки',
                    max_length: 255,
                    prefix: '@',
                    sofix: '@',
                    normalizers:[
                      { normalizer: 'lowercase' },
                      { normalizer: 'string_length', min: 5, max: 10 },
                      { normalizer: 'email_format' },
                    ]
                  },
                  {
                    component: 'IntegerField',
                    verbose_name: 'Счётчик',
                    name: 'field_2',
                    help_text: 'Какой-то счётчик',
                    max: 255,
                    step: 1,
                    prefix: '',
                    sofix: '$'
                  },
                  {
                    component: 'IfBlock',
                    id: 'if_block',
                    condition: 'field_1=="find@me.ru" and field_2==2',
                    then: [
                      {
                        component: 'BooleanField',
                        verbose_name: 'Спрятанный Чекбокс',
                        name: 'field_0'
                      },
                    ]
                  },
                  {
                    component: 'BooleanField',
                    verbose_name: 'Чекбокс',
                    name: 'field_3'
                  },
                  {
                    component: 'FloatField',
                    verbose_name: 'Float',
                    name: 'field_4'
                  },
                  {
                    component: 'TextField',
                    verbose_name: 'Text',
                    name: 'field_5',
                    normalizers:[
                      { normalizer: 'uppercase' },
                      { normalizer: 'string_length', min: 5, max: 110 }
                    ]
                  },
                ]
              }
            ]					
          },
          {
            verbose_name: 'Вкладка 2',
            childs: [
              {
                component: 'StringField',
                verbose_name: 'Адрес',
                name: 'field_6',
                help_text: 'Укажите полный адрес доставки',
                max_length: 255,
                prefix: '@',
                sofix: ''
              },
              {
                component: 'TextField',
                verbose_name: 'Text',
                name: 'field_7',
                normalizers:[
                  { normalizer: 'uppercase' },
                  { normalizer: 'string_length', min: 5, max: 110 }
                ]
              },
            ]
          }
        ]
      }
    ]
  }
});
