import React from 'react';
import { connect } from 'react-redux';
import { Navbar, NavbarBrand, Container, Row, Col, Jumbotron, Alert } from 'reactstrap';

import { onSubmit, hideAlert, setFormValues, ImmutableState } from './duck';
import { State } from '../../store/rootReducer';

import InputForm from '../InputForm';
import FormBuilder from '../../../lib/FormBuilder';


export type StateProps = ImmutableState;
export type DispatchProps = {
  onSubmit: (inputString: string) => void
  hideAlert: () => void
  setFormValues: (values: any) => void
};
export type ComponentProps = {};
export type Props = StateProps & DispatchProps & ComponentProps;


class App extends React.Component<Props> {
  private onDismiss = () => {
    this.props.hideAlert();
  }

  private onJsonSubmit = (value: { input_string: string }) => {
    this.props.onSubmit(value.input_string);
  }

  private handleFromSubmit = (values: any) => {
    this.props.setFormValues(values);
  }

  render() {
    const { json, error, errorMsg, formValues } = this.props;
    const { struct, initial, custom_args } = json;
    const initialValues = { ...initial, ...custom_args };

    return (
      <div>
        <Navbar light color="light">
          <NavbarBrand href="/">FormBuilder component</NavbarBrand>
        </Navbar>

        <br/>

        <Container>
          <Alert color="danger" isOpen={error} toggle={this.onDismiss}>
            {errorMsg}
          </Alert>

          {formValues && <pre>{JSON.stringify(formValues, null, 2)}</pre>}

          <Row>
            <Col xs="7">
              <Jumbotron>
                <InputForm
                  initialValues={{ input_string: JSON.stringify(json, null, 2) }}
                  onSubmit={this.onJsonSubmit}/>
              </Jumbotron>  
            </Col>
            <Col xs="5">
              <FormBuilder
                form="new_form"
                schema={struct}
                enableReinitialize
                initialValues={initialValues}
                onSubmit={this.handleFromSubmit}/>
            </Col>    
          </Row>
        </Container>  
      </div>
    );
  }
}

export default connect<StateProps, DispatchProps, ComponentProps, State>(
  state => state.app,
  { onSubmit, hideAlert, setFormValues }
)(App);
