import createReducer, { Action } from '../../utils/createReducer';
import Immutable from 'seamless-immutable';
import { ThunkAction } from 'redux-thunk';
import { State as GlobalState } from '../../store/rootReducer';
import { initialState } from './initialState';

/**
 * Consts
 */
export const UPDATE_STATE = 'app/UPDATE_STATE';
export const SET_ERR_MSG = 'app/SET_ERROR_MSG';
export const SHOW_ALERT = 'app/SHOW_ALERT';
export const HIDE_ALERT = 'app/HIDE_ALERT';
export const SET_FORM_VALUES = 'app/SET_FORM_VALUES';


/**
 * Actions creators
 */
export function updateState(state: JsonState): Action<JsonState> {
  return {
    type: UPDATE_STATE,
    payload: state
  };
}

export function setErrorMsg(msg: string): Action<string> {
  return {
    type: SET_ERR_MSG,
    payload: msg
  };
}

export function showAlert(): Action<void> {
  return {
    type: SHOW_ALERT
  };
}

export function hideAlert(): Action<void> {
  return {
    type: HIDE_ALERT
  };
}

export function onSubmit(inputString: string): ThunkAction<void, GlobalState, void> {
  return (dispatch) => {
    const initJson: JsonState = {
      handler: '',
      initial: {},
      custom_args: {},
      struct: [],
      buttons: {
        submit: ''
      }
    };

    try {
      const json = JSON.parse(inputString);
      dispatch(setErrorMsg(''));
      dispatch(hideAlert());
      dispatch(updateState({ ...initJson, ...json }));  
    } catch (e) {
      dispatch(setErrorMsg(e.message));
      dispatch(showAlert());
    }
  };
}

export function setFormValues(values: any): Action<any> {
  return {
    type: SET_FORM_VALUES,
    payload: values
  };
}


/**
 * Types
 */
export type FieldMap = { [fieldName: string]: any };
export type ButtonMap = {
  submit: string
  cancel?: string
  delete?: string
};

export type JsonState = {
  handler: string
  struct: any[]
  initial: FieldMap 
  custom_args: FieldMap
  buttons: ButtonMap
};

export type State = {
  formValues: any
  error: boolean
  errorMsg: string
  json: JsonState
};

export type ImmutableState = Immutable.ImmutableObject<State>;


/**
 * App reducer
 */
export default createReducer<ImmutableState>({
  [UPDATE_STATE]: (state, { payload }) => state.set('json', payload),

  [SET_ERR_MSG]: (state, { payload }) => state.set('errorMsg', payload),

  [SHOW_ALERT]: state => state.set('error', true),

  [HIDE_ALERT]: state => state.set('error', false).set('errorMsg', ''),

  [SET_FORM_VALUES]: (state, { payload }) => state.set('formValues', payload)
}, initialState);
