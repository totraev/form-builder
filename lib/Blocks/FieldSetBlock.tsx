import React from 'react';
import { FormGroup } from 'reactstrap';
import { BlockComponentProps } from '../formBlock';

type Props = BlockComponentProps & {
  name: string
  legend?: string
  childs: any[]
};

export const FieldSetBlock: React.SFC<Props> = ({ legend, childs, renderChildren }) => {
  return (
    <FormGroup>
      {legend !== null && <legend className="col-form-label">{legend}</legend>}
      {renderChildren(childs)}
    </FormGroup>
  );
};

FieldSetBlock.defaultProps = {
  legend: null
};

export default FieldSetBlock;
