import React from 'react';
import { FormGroup } from 'reactstrap';
import { BlockComponentProps } from '../formBlock';
import compileExpression from 'filtrex';


type Props = BlockComponentProps & {
  name: string
  condition: string
  then: any[]
};


export const IfBlock: React.SFC<Props> = (props) => {
  const { condition, then, renderChildren, values } = props;
  const visible = Boolean(compileExpression(condition)(values));

  return (visible &&
    <FormGroup>
      {renderChildren(then)}
    </FormGroup>
  );
};

IfBlock.defaultProps = {
  condition: '',
  then: [],
  values: {}
};

export default IfBlock;
