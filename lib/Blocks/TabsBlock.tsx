import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classNames from 'classnames';
import { BlockComponentProps } from '../formBlock';
import FontAwesome from 'react-fontawesome';


export type Tab = {
  verbose_name?: string
  icon?: string
  childs: any[]
};
export type Props = BlockComponentProps & {
  name: string
  tabs?: Tab[]
};
export type State = {
  activeTab: number
};


class TabsBlock extends React.Component<Props, State> {
  state: State = {
    activeTab: 0
  };

  toggle = (tabIndex: number) => () => {
    this.setState({ activeTab: tabIndex });
  }

  render() {
    const { tabs, id, renderChildren } = this.props;
    const { activeTab } = this.state;

    return (
      <div id={id}>
        <Nav tabs>
          {tabs.map(({ verbose_name, icon }, i) => (
            <NavItem key={`${id}-nav-${i}`}>
              <NavLink
                className={classNames({ active: activeTab === i })}
                onClick={this.toggle(i)}>

                {icon !== undefined && <FontAwesome name={icon}/>}
                {icon !== undefined ? ` ${verbose_name}` : verbose_name}
              </NavLink>
            </NavItem>
          ))}
        </Nav>

        <TabContent activeTab={activeTab}>
          {tabs.map(({ childs }, i) => (
            <TabPane key={`${id}-tab-${i}`} tabId={i}>
              {renderChildren(childs)}
            </TabPane>
          ))}
        </TabContent>
      </div>
    );
  }
}

export default TabsBlock;
