import React from 'react';
import { FieldProps } from '../reduxFormField';
import { WrappedFieldProps } from 'redux-form';
import { 
  InputGroup,
  InputGroupAddon, 
  Input,
  FormGroup,
  FormText,
  Label,
  FormFeedback
} from 'reactstrap';


export type IntegerField = {
  prefix?: string
  sofix?: string
  min?: number
  max?: number
  step?: number
};

export type Props = FieldProps & IntegerField & WrappedFieldProps;


const IntegerField: React.SFC<Props> = (props) => {
  const {
    name,
    verbose_name,
    help_text,
    prefix,
    sofix,
    min,
    max,
    step,
    input,
    meta
  } = props;

  return (
    <FormGroup>
      {verbose_name !== '' && <Label for={name}>{verbose_name}</Label>}
      <InputGroup>
        {prefix !== '' && <InputGroupAddon addonType="prepend">{prefix}</InputGroupAddon>}
        <Input
          placeholder={verbose_name}
          id={name}
          name={name}
          type="number"
          min={min}
          max={max}
          step={step}
          valid={meta.valid ? undefined : false}
          {...input}/>
        {sofix !== '' && <InputGroupAddon addonType="append">{sofix}</InputGroupAddon>}
      </InputGroup>
      {meta.invalid && <FormFeedback>{meta.error}</FormFeedback>}
      {help_text !== '' && <FormText color="muted">{help_text}</FormText>}
    </FormGroup>
  );
};

IntegerField.defaultProps = {
  required: false,
  readonly: false,
  verbose_name: '',
  help_text: '',
  prefix: '',
  sofix: '',
  min: null,
  max: null,
  step: null
};

export default IntegerField;
