import React from 'react';
import { FieldProps } from '../reduxFormField';
import { WrappedFieldProps } from 'redux-form';
import { 
  InputGroup,
  InputGroupAddon, 
  Input,
  FormGroup,
  FormText,
  Label,
  FormFeedback
} from 'reactstrap';


export type StringField = {
  max_length?: number
  prefix?: string
  sofix?: string
};
export type Props = FieldProps & StringField & WrappedFieldProps;


const StringField: React.SFC<Props> = (props) => {
  const {
    name,
    max_length,
    verbose_name,
    help_text,
    prefix,
    sofix,
    input,
    meta
  } = props;

  return (
    <FormGroup>
      {verbose_name !== '' && <Label for={name}>{verbose_name}</Label>}
      <InputGroup>
        {prefix !== '' && <InputGroupAddon addonType="prepend">{prefix}</InputGroupAddon>}
        <Input
          placeholder={verbose_name}
          id={name}
          name={name}
          valid={meta.valid ? undefined : false}
          maxLength={max_length}
          type="string"
          {...input}/>
        {sofix !== '' && <InputGroupAddon addonType="append">{sofix}</InputGroupAddon>}
        {meta.invalid && <FormFeedback>{meta.error}</FormFeedback>}
      </InputGroup>
      {help_text !== '' && <FormText color="muted">{help_text}</FormText>}
    </FormGroup>
  );
};

StringField.defaultProps = {
  required: false,
  readonly: false,
  verbose_name: '',
  help_text: '',
  max_length: null,
  prefix: '',
  sofix: ''
};

export default StringField;
