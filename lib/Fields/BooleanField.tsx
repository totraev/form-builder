import React from 'react';
import { FieldProps } from '../reduxFormField';
import { WrappedFieldProps } from 'redux-form';
import { 
  Input,
  FormGroup,
  Label
} from 'reactstrap';


export type BooleanField = {};
export type Props = FieldProps & BooleanField & WrappedFieldProps;


const BooleanField: React.SFC<Props> = (props) => {
  const {
    name,
    verbose_name,
    input
  } = props;

  return (
    <FormGroup check>
      <Label check for={name}>
        <Input id={name} name={name} {...input} type="checkbox"/>
        {verbose_name !== '' ? ` ${verbose_name}` : ''}
      </Label>
    </FormGroup>
  );
};

BooleanField.defaultProps = {
  required: false,
  readonly: false,
  verbose_name: '',
  help_text: ''
};

export default BooleanField;
