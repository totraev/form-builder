import React from 'react';
import { FieldProps } from '../reduxFormField';
import { WrappedFieldProps } from 'redux-form';
import { 
  InputGroup,
  InputGroupAddon, 
  Input,
  FormGroup,
  FormText,
  Label,
  FormFeedback
} from 'reactstrap';


export type FloatField = {
  prefix?: string
  sofix?: string
};

export type Props = FieldProps & FloatField & WrappedFieldProps;


const FloatField: React.SFC<Props> = (props) => {
  const {
    name,
    verbose_name,
    help_text,
    prefix,
    sofix,
    input,
    meta
  } = props;

  return (
    <FormGroup>
      {verbose_name !== '' && <Label for={name}>{verbose_name}</Label>}
      <InputGroup>
        {prefix !== '' && <InputGroupAddon addonType="prepend">{prefix}</InputGroupAddon>}
        <Input
          placeholder={verbose_name}
          id={name}
          name={name}
          valid={meta.valid ? undefined : false}
          {...input}
          type="number"
          step="any"/>
        {sofix !== '' && <InputGroupAddon addonType="append">{sofix}</InputGroupAddon>}
      </InputGroup>
      {meta.invalid && <FormFeedback>{meta.error}</FormFeedback>}
      {help_text !== '' && <FormText color="muted">{help_text}</FormText>}
    </FormGroup>
  );
};

FloatField.defaultProps = {
  required: false,
  readonly: false,
  verbose_name: '',
  help_text: '',
  prefix: '',
  sofix: ''
};

export default FloatField;
