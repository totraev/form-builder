import React from 'react';
import { FieldProps } from '../reduxFormField';
import { WrappedFieldProps } from 'redux-form';
import { 
  Input,
  FormGroup,
  FormText,
  Label,
  FormFeedback
} from 'reactstrap';


export type TextField = {};
export type Props = FieldProps & TextField & WrappedFieldProps;


const TextField: React.SFC<Props> = (props) => {
  const {
    name,
    verbose_name,
    help_text,
    input,
    meta
  } = props;

  return (
    <FormGroup>
      {verbose_name !== '' && <Label for={name}>{verbose_name}</Label>}
      <Input
        placeholder={verbose_name}
        id={name}
        name={name}
        type="textarea"
        valid={meta.valid ? undefined : false}
        {...input}/>
      {meta.invalid && <FormFeedback>{meta.error}</FormFeedback>}
      {help_text !== '' && <FormText color="muted">{help_text}</FormText>}
    </FormGroup>
  );
};

TextField.defaultProps = {
  required: false,
  readonly: false,
  verbose_name: '',
  help_text: ''
};

export default TextField;
