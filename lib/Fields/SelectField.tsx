import React from 'react';
import { FieldProps } from '../reduxFormField';
import { WrappedFieldProps } from 'redux-form';
import { 
  Input,
  FormGroup,
  FormText,
  Label,
  FormFeedback
} from 'reactstrap';


export type SelectField = {
  values?: { [val: string]: string }
};

export type Props = FieldProps & SelectField & WrappedFieldProps;


const SelectField: React.SFC<Props> = (props) => {
  const {
    name,
    verbose_name,
    help_text,
    values,
    input,
    meta
  } = props;

  return (
    <FormGroup>
      {verbose_name !== '' && <Label for={name}>{verbose_name}</Label>}
      <Input 
        placeholder={verbose_name}
        id={name}
        name={name}
        type="select"
        valis={meta.valid ? undefined : false}
        {...input}>

        {Object.entries(values).map(([value, name]) => <option value={value}>{name}</option>)}
      </Input>
      {meta.invalid && <FormFeedback>{meta.error}</FormFeedback>}
      {help_text !== '' && <FormText color="muted">{help_text}</FormText>}
    </FormGroup>
  );
};

SelectField.defaultProps = {
  required: false,
  readonly: false,
  verbose_name: '',
  help_text: '',
  values: {}
};

export default SelectField;
