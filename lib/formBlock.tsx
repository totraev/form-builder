import React from 'react';
import { getFormValues } from 'redux-form';
import { connect } from 'react-redux';


export type ComponentType =
  React.ComponentClass<any> |
  React.SFC<any>;

export type BlockProps = {
  id: string
};

export type BlockOptions = {
  renderChildren: (children: any) => JSX.Element[],
  name: string,
};

export type BlockComponentProps = BlockProps & BlockOptions & { values: any };


const formBlock = ({ renderChildren, name }: BlockOptions) => (Component: ComponentType) => {
  class BlockComponent extends React.PureComponent<BlockProps & { values: any }> {
    render() {
      const { id, ...props } = this.props;
      
      return <Component
        id={id}
        {...props}
        renderChildren={renderChildren}
      />;
    }
  }
  
  return Component.name === 'IfBlock'
    ? connect<{ values: any }, {}, BlockProps>(
        state => ({ values: getFormValues(name)(state) })
      )(BlockComponent)
    : BlockComponent;
};

export default formBlock;
