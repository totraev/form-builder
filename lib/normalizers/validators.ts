export const validator = {
  email_format: () => (value?: string): string => 
    Boolean(value) && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
      ? 'Invalid email address'
      : undefined,
  
  requered: () => (value?: string): string => Boolean(value) ? undefined : 'Field is requered',
  
  string_length: ({ min, max }: { min: number, max: number }) => (value?: string): string => { 
    return Boolean(value) && (value.length < min || value.length > max)
      ? `Invalid string length: min ${min}, max ${max}`
      : undefined;
  },

  getValidators: (props = []) => {
    return props
      .map(({ normalizer: n, ...props }) => validator[n] && validator[n](props))
      .filter(normalizer => normalizer);
  }    
};
