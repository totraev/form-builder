export const normalizer = {
  uppercase: (value: string) => Boolean(value) ? value.toUpperCase() : '',

  lowercase: (value?: string): string => Boolean(value) ? value.toLowerCase() : '',

  capfirst: (value?: string): string => Boolean(value)
      ? value.charAt(0).toUpperCase() + value.slice(1).toLowerCase()
      : '',
  
  getNormalizer: (props = []) => {
    const normalizers = props
      .map(({ normalizer: n }) => normalizer[n])
      .filter(normalizer => normalizer);

    if (normalizers.length > 0) {
      return value => normalizers.reduce((value, normalizer) => normalizer(value), value);
    }  
  }
};
