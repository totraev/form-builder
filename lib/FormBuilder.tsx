import React from 'react';
import { Form, reduxForm, InjectedFormProps } from 'redux-form';
import { Button } from 'reactstrap';  

import reduxFormField from './reduxFormField';
import formBlock from './formBlock';

import {
  TabsBlock,
  FieldSetBlock,
  IfBlock
} from './Blocks';

import { 
  StringField,
  IntegerField,
  FloatField,
  TextField, 
  BooleanField, 
  SelectField,
} from './Fields';


export type Component = React.ComponentClass<any> | React.SFC<any>;
export type ComponentMap = { [componentName: string]: Component };
export type ComponentProps = {
  schema: any[]
  fields?: Component[]
  blocks?: Component[]
  buttons?: {
    submit: string
  }
};
export type Props = ComponentProps & InjectedFormProps<any, ComponentProps>;


const fields: Component[] = [
  StringField,
  IntegerField,
  FloatField,
  TextField,
  BooleanField,
  SelectField
];

const blocks: Component[] = [
  FieldSetBlock,
  TabsBlock,
  IfBlock
];

class FormBuilder extends React.PureComponent<Props> {
  static defaultProps: Readonly<ComponentProps> = {
    fields,
    blocks,
    schema: [],
    buttons: {
      submit: 'Submit'
    }
  };
  componentMap: ComponentMap = {};

  constructor(props) {
    super(props);
    this.componentMap = this.getComponentMap(props);
  }

  componentWillUpdate(nextProps: Readonly<Props>) {
    const { fields, blocks } = this.props;

    if (nextProps.fields !== fields && nextProps.blocks !== blocks) {
      this.componentMap = this.getComponentMap(nextProps);
    }
  }

  private getComponentMap = (props: Props) => {
    const { fields, blocks, form } = props;
   
    const wrapFormBlock = formBlock({
      renderChildren: this.renderChildren,
      name: form
    });

    const fieldsMap = fields.reduce((componentMap, component) => {
      componentMap[component.name] = reduxFormField(component);
      return componentMap;
    }, {});

    const blockMap = blocks.reduce((componentMap, component) => {
      componentMap[component.name] = wrapFormBlock(component);
      return componentMap;
    }, {});

    return { ...fieldsMap, ...blockMap };
  }

  private renderComponent = (componentName: string, componentProps: any) => {
    const Component = this.componentMap[componentName];

    if (Component === undefined) {
      throw Error(`Component ${componentName} is not defined in FromBuilder`);
    }

    return <Component
      key={componentProps.name || componentProps.id}
      {...componentProps}/>;
  }

  private renderChildren = (children: any[] = []) => {
    return children.map(({ component, ...props }) => this.renderComponent(component, props));
  }

  render() {
    const { schema, handleSubmit, buttons } = this.props;
    const components = Array.isArray(schema) ? schema : [schema];
  
    return (
      <Form onSubmit={handleSubmit}>
        {this.renderChildren(components)}

        <Button type="submit">{buttons.submit}</Button>
      </Form>
    );
  }
}

export default reduxForm<any, ComponentProps>({})(FormBuilder);
