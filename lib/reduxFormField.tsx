import React from 'react';
import { Field as BaseField, BaseFieldProps, WrappedFieldProps } from 'redux-form';
import { normalizer } from './normalizers/normalizers';
import { validator } from './normalizers/validators';



export type ComponentType =
  React.ComponentClass<FieldComponentProps & WrappedFieldProps> |
  React.SFC<FieldComponentProps & WrappedFieldProps>;

export type Normalizer = {
  validator: string
  [name: string]: boolean | string | number
};

export type FieldProps = {
  name: string  
  verbose_name?: string
  required?: boolean
  readonly?: boolean
  help_text?: string
};

export type FieldComponentNormalizers = {
  normalizers?: Normalizer[]
};

export type FieldComponentProps = FieldComponentNormalizers & FieldProps;

class Field extends BaseField<BaseFieldProps<FieldProps>> {}


const reduxFormField = (Component: ComponentType) => {
  return  class FieldComponent extends React.PureComponent<FieldComponentProps> {
    render() {
      const { normalizers, ...componentProps } = this.props;

      const normalize = normalizer.getNormalizer(normalizers);
      const validate = validator.getValidators(normalizers);

      return <Field
        name={componentProps.name}
        validate={validate}
        normalize={normalize}
        props={componentProps}
        component={Component}/>;
    }
  };
};

export default reduxFormField;


